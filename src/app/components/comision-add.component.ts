import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';

import { ComisionSV } from '../services/comision.service';
import { Comision } from '../models/comision';

@Component({
	selector: 'comision-add',
	templateUrl: '../views/comision-add.component.html',
	providers: [ComisionSV]
})

export class ComisionAddComponent{

	public comision : Comision;

	constructor(
		private _comisionSV: ComisionSV,
		private _router: Router,
		private alertService: SweetAlertService
		
	){
		this.comision = new Comision(null,null,null,null,true);
	}

	ngOnInit(){
	}

	onSubmit(){
		console.log(this.comision);

		this._comisionSV.addComision(this.comision).subscribe(
			response =>{
				console.log("Guardado:D");
			},
			error =>{
				console.log(<any>error);
				this.alertService.success({
	        			title: '¡La comisión fue guardada exitosamente!'
	      			});
					this._router.navigate(['/comisiones']);	
			}
		);
	}
}