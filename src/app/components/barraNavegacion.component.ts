import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SolicitudSV } from '../services/solicitud.service';
import { Solicitud } from '../models/solicitud';

import { LoginSV } from '../services/login.service';
import { EmpresaSV } from '../services/empresa.service';

@Component({
	selector: 'barraNavegacion',
	templateUrl: '../views/barraNavegacion.component.html',
	providers: [SolicitudSV, LoginSV, EmpresaSV]
})

export class BarraNavegacionComponent{
	public number2: number;
	public listaSolicitudesPendientes : Solicitud[];
	public usuario : string;

	constructor(
		private _solicitudSV: SolicitudSV,
		private _loginSV: LoginSV,
		private _empresaSV: EmpresaSV,
		private _router: Router,
	){
		this.usuario = sessionStorage.getItem("user");
	}

	ngOnInit(){
		this.getSolicitudesPendientes();
	}

	getSolicitudesPendientes(){
		this._solicitudSV.getSolicitudesPendientes().subscribe(
			result => {
				this.listaSolicitudesPendientes = result;
				this.number2 = this.listaSolicitudesPendientes.length;
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	updateSolicitudesPendientes(solicitudesPendientes){
		this.number2 = this.listaSolicitudesPendientes.length;
	}

	logOut(){
		localStorage.removeItem('token');
		// console.log("Eliminado?",localStorage.getItem('token'));
		this._router.navigate(['/login']);
	}
}

