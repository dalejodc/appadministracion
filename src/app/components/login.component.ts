import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { User } from '../models/user';

import { LoginSV } from '../services/login.service';

@Component({
	selector: 'login',
	templateUrl: '../views/login.component.html',
	providers: [LoginSV]
})

export class LoginComponent{

	public user: User;
	public errMsg: boolean;
	public advertencia="Error, las credenciales son incorrectas.";

	constructor(
		private _loginSV: LoginSV,
		private _router: Router
		){
		this.user = new User(null, null, null, null);
	}

	ngOnInit(){
	}

	login2(){
		this._loginSV.login(this.user);
		this.errMsg = this._loginSV.getMensajeError();
		console.log(this.errMsg);
		if(this.errMsg) {		//Si ocurre un error, reinicia las credenciales
			this.user = new User(null, null, null, null);
		}
	}
}
