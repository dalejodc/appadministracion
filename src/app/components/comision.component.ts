import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';

import { ComisionSV } from '../services/comision.service';
import { Comision } from '../models/comision';

@Component({
	selector: 'comisiones',
	templateUrl: '../views/comision-list.component.html',
	providers: [ComisionSV]
})

export class ComisionComponent{

	public listaComisiones : Comision[];
	public listaComisionesDesactivadas : Comision[];
	public comision: Comision;

	constructor(
		private _comisionSV: ComisionSV,
		private alertService: SweetAlertService
	){}

	ngOnInit(){
		this.getComisiones();
		this.getComisionesDesactivadas();

	}

	getComisiones(){
		this._comisionSV.getComisionesActivas().subscribe(
			result => {
				// console.log(result);
				this.listaComisiones = result;
				console.log("lista pendientes:",this.listaComisiones);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getComisionesDesactivadas(){
		this._comisionSV.getComisionesDesactivadas().subscribe(
			result => {
				// console.log(result);
				this.listaComisionesDesactivadas = result;
				console.log("lista Comisiones desactivadas:",this.listaComisionesDesactivadas);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	deshabilitar(comision){
		this.alertService.confirm({
	      title: '¿Desea deshabilitar el plan '+comision.nombre+'?',
	      text: "El plan pasará a estado deshabilitado.",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(() => {
		    this.comision = comision;
			this.comision.estado =false;

			this._comisionSV.editComision(comision.id, this.comision).subscribe(
				response =>{
					console.log("Actualizado");
				},
				error =>{
					// console.log(this.comision.estado);
					console.log(<any>error);
					this.ngOnInit();
				}
			);	
	      this.alertService.success({
	        title: '¡El plan '+ comision.nombre+' fue deshabilitado exitosamente!'
	      });
	    })

	    .catch(() => console.log('canceled'));
	}

	habilitar(comision){
		this.alertService.confirm({
	      title: '¿Desea habilitar el plan '+comision.nombre+'?',
	      text: "El plan pasará a estado desactivado.",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(() => {
		    this.comision = comision;
			this.comision.estado =true;

			this._comisionSV.editComision(comision.id, this.comision).subscribe(
				response =>{
					console.log("Actualizado");
				},
				error =>{
					// console.log(this.comision.estado);
					console.log(<any>error);
					this.ngOnInit();
				}
			);	
	      this.alertService.success({
	        title: '¡El plan '+comision.nombre+' se habilitado!'
	      });
	    })

	    .catch(() => console.log('canceled'));
	}
}