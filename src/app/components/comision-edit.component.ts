import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';
import { Router, ActivatedRoute, Params} from '@angular/router';

import { ComisionSV } from '../services/comision.service';
import { Comision } from '../models/comision';

@Component({
	selector: 'comision-edit',
	templateUrl: '../views/comision-edit.component.html',
	providers: [ComisionSV]
})

export class ComisionEditComponent{
	public comision : Comision;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _comisionSV: ComisionSV,
		private alertService: SweetAlertService
	){
		this.comision = new Comision(null,null, null, null, true);
	}

	ngOnInit(){
		this.getComision();
	}

	getComision(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._comisionSV.getComision(id).subscribe(
				response=>{
						this.comision= response;
						console.log(this.comision);
				},
				error=>{
					console.log(<any>error);
				}
			);
		});
	}

	onSubmit(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			this._comisionSV.editComision(id, this.comision).subscribe(
				response =>{
				},
				error =>{
					this.alertService.success({
	        			title: '¡El plan fue editado exitosamente!'
	      			});
					this._router.navigate(['/comisiones']);				}
			);
		});	
	}
}