import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';
import { Router, ActivatedRoute, Params} from '@angular/router';

import { ComisionSV } from '../services/comision.service';
import { Comision } from '../models/comision';

import { DepartamentoSV } from '../services/departamento.service';
import { Departamento } from '../models/departamento';

import { MunicipiosSV } from '../services/municipios.service';
import { Municipio } from '../models/municipio';

import { Empresa } from '../models/empresa'
import { EmpresaSV } from '../services/empresa.service';

import { Representante } from '../models/representante'

import { User } from '../models/user';
import { Authorities } from '../models/authorities';

@Component({
	selector: 'empresa-edit',
	templateUrl: '../views/empresa-edit.component.html',
	providers: [DepartamentoSV, MunicipiosSV, ComisionSV, EmpresaSV]
})

export class EmpresaEditComponent{

	public empresa: Empresa;
	public municipio: Municipio;
	public representante: Representante;
	public comision: Comision;
	public departamento: Departamento;

	public user: User;
	public authorities: Authorities;

	public listaComisiones : Comision[];
	public listaMunicipios : Municipio[];
	public listaDepartamentos : Departamento[];

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _departamentoSV: DepartamentoSV,
		private _municipiosSV: MunicipiosSV,
		private _comisionSV: ComisionSV,
		private _empresaSV: EmpresaSV,
		private alertService: SweetAlertService,
	){
		this.representante = new Representante(null, null, null, null, null, true, this.user);
		this.departamento = new Departamento(null, null);
		this.municipio = new Municipio(0, null,this.departamento);
		this.comision = new Comision(null, null, null, null, true);
		this.empresa = new Empresa(null, null, null, null, true, this.comision, this.municipio, this.representante, null);
	}

	ngOnInit(){
		this.getEmpresa();
		this.getDepartamentos();
		this.getComisiones();
	}

	onSubmit(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			this._empresaSV.editEmpresa(id, this.empresa).subscribe(
				response =>{
				},
				error =>{
					this.alertService.success({
	        			title: '¡La empresa fue editada exitosamente!'
	      			});
					this._router.navigate(['/empresas']);				}
			);
		});	
	}

	getEmpresa(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];

			this._empresaSV.getEmpresa(id).subscribe(
				response=>{
						console.log("Empresa traida:", response);
						this.empresa= response;
						this.representante = response.representante;
						this.municipio = response.municipio;
						this.comision = response.comision;
						// this.departamento = response.departamento;
						this.departamento.depNombre = response.municipio.departamento.depNombre;
						console.log(response.comision.nombre);
						console.log(response.municipio.munNombre);
						this.departamento.depCodigo = response.municipio.depCodigo;
						this.getMunicipios(this.departamento.depCodigo);
				},
				error=>{
					console.log(<any>error);
				}
			);
		});
	}

	getComisiones(){
		this._comisionSV.getComisionesActivas().subscribe(
			result => {
				console.log(result);
				this.listaComisiones = result;
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getDepartamentos(){
		this._departamentoSV.getDepartamentos().subscribe(
			result => {
				this.listaDepartamentos = result;
				console.log(result);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getMunicipios(id){
		this._municipiosSV.getMunicipios(id).subscribe(
			result => {
				this.listaMunicipios = result;
			},
			error => {
				console.log(<any>error);
			}
			);
	}
}