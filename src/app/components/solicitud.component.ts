import { Component } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { SweetAlertService } from 'angular-sweetalert-service';

import { SolicitudSV } from '../services/solicitud.service';
import { Solicitud } from '../models/solicitud';

@Component({
	selector: 'solictudes',
	templateUrl: '../views/solicitud-list.component.html',
	providers: [SolicitudSV]
	})

export class SolicitudComponent{
	public listaSolicitudesPendientes : Solicitud[];
	public listaSolicitudesRechazadas : Solicitud[];
	public solicitud: Solicitud;
	public xd: number;

	constructor(
		private _solicitudSV: SolicitudSV,
		private _router: Router,
		private alertService: SweetAlertService
		){}

	ngOnInit(){
	 this.getSolicitudesPendientes();
	 this.getSolicitudesRechazadas();
	}

	deleteSolicitud(id){
		this.xd  = id;
		this.alertService.confirm({
	      title: '¿Desea eliminar permanente la solicitud '+this.xd+'?',
	      text: "La operación no se podrá revertir",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(() => {
			this._solicitudSV.deleteSolicitud(id).subscribe(
				response =>{
					this.ngOnInit();
				},
				error =>{
					console.log(<any>error);
					this.ngOnInit();
				}
			);	
	      this.alertService.success({
	        title: '¡Solicitud elimminada exitosamente!'
	      });
	    })

	    .catch(() => console.log('canceled'));
	}

	rechazarSolicitud(id, sol){
		// private id: number;
		this.xd  = id;
		this.alertService.confirm({
	      title: '¿Desea rechazar la solicitud '+this.xd+'?',
	      text: "La acción no se podrá revertir",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(() => {
		    this.solicitud = sol;
			this.solicitud.estado.id = 3;

			this._solicitudSV.editSolicitud(id, this.solicitud).subscribe(
				response =>{
					console.log("Actualizado");
				},
				error =>{
					console.log(<any>error);
					this.ngOnInit();
					// this._router.navigate(['/solicitudes']);
				}
			);	
	      this.alertService.success({
	        title: '¡Solicitud '+ this.xd+' fue rechazada exitosamente!'
	      });
	    })

	    .catch(() => console.log('canceled'));
	}

	getSolicitudesPendientes(){
		this._solicitudSV.getSolicitudesPendientes().subscribe(
			result => {
				// console.log(result);
				this.listaSolicitudesPendientes = result;
				console.log("lista pendientes:",this.listaSolicitudesPendientes);
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getSolicitudesRechazadas(){
		this._solicitudSV.getSolicitudesRechazadas().subscribe(
			result => {
				// console.log(result);
				this.listaSolicitudesRechazadas = result;
				console.log("lista rechazadas:",this.listaSolicitudesRechazadas);
			},
			error => {
				console.log(<any>error);
			}
			);
	}
}

