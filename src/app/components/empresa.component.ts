import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';


import { EmpresaSV } from '../services/empresa.service';
import { Empresa } from '../models/empresa';

@Component({
	selector: 'empresas',
	templateUrl: '../views/empresa-list.component.html',
	providers: [EmpresaSV]

})

export class EmpresaComponent{

	public listaEmpresas : Empresa[];

	constructor(
		private _empresaSV : EmpresaSV,
		private alertService: SweetAlertService
	){}

	ngOnInit(){
		this.getEmpresas();
	}

	getEmpresas(){
		this._empresaSV.getEmpresas().subscribe(
			result => {
				console.log(result);
				this.listaEmpresas = result;
			},
			error => {
				console.log("Error al traer las empresas:",<any>error);
			}
			);
	}

	eliminarEmpresa(id, nombre){
		this.alertService.confirm({
	      title: '¿Desea eliminar permanente la empresa '+nombre+'?',
	      text: "La operación no se podrá revertir en el sistema",
	      cancelButtonColor: '#E57373',
  		  confirmButtonColor: '#83C283',
  		  confirmButtonText: 'Aceptar'
	    })
	    .then(() => {
			this._empresaSV.deleteEmpresa(id).subscribe(
				response =>{
					this.ngOnInit();
				},
				error =>{
					console.log(<any>error);
					this.ngOnInit();
				}
			);	
	      this.alertService.success({
	        title: '¡Empresa'+ nombre+' eliminada exitosamente!'
	      });
	    })

	    .catch(() => console.log('canceled'));
	}
}