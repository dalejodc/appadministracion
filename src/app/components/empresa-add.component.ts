import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';
import { Router, ActivatedRoute, Params} from '@angular/router';

import { ComisionSV } from '../services/comision.service';
import { DepartamentoSV } from '../services/departamento.service';
import { MunicipiosSV } from '../services/municipios.service';
import { EmpresaSV } from '../services/empresa.service';

import { Representante } from '../models/representante';
import { Departamento } from '../models/departamento';
import { Municipio } from '../models/municipio';
import { Comision } from '../models/comision';
import { Empresa } from '../models/empresa';
import { User } from '../models/user';
import { Authorities } from '../models/authorities';
import { Inventario } from '../models/inventario';

@Component({
	selector: 'empresa-add',
	templateUrl: '../views/empresa-add.component.html',
	providers: [ComisionSV, DepartamentoSV, MunicipiosSV, EmpresaSV]
})

export class EmpresaAddComponent{

	public empresa: Empresa;
	public municipio: Municipio;
	public representante: Representante;
	public comision: Comision;
	public departamento: Departamento;
	public user: User;
	public listaAuthorities: Authorities[];
	public authorities: Authorities;
	public inventario: Inventario;
	public passAux: string;


	public listaComisiones : Comision[];
	public listaMunicipios : Municipio[];
	public listaDepartamentos : Departamento[];

	constructor(
		private _comisionSV: ComisionSV,
		private _departamentoSV: DepartamentoSV,
		private _municipiosSV: MunicipiosSV,
		private _empresaSV: EmpresaSV,
		private alertService: SweetAlertService,
		private _router: Router
	){
		this.authorities = new Authorities(1, "ROLE_USER");
		this.listaAuthorities = [this.authorities];
		this.inventario = new Inventario(null, null);
		this.user = new User(null, null, true, this.listaAuthorities);
		this.representante = new Representante(null, null, null, null, null, true, this.user);
		this.departamento = new Departamento(null, null);
		this.municipio = new Municipio(0, null,this.departamento);
		this.comision = new Comision(null, null, null, null, true);
		this.empresa = new Empresa(null, null, null, null, true, this.comision, this.municipio, this.representante, this.inventario);
	}

	ngOnInit(){
		this.getComisiones();
		this.getDepartamentos();
	}

	onSubmit(){
		console.log(this.empresa);
		let jsonempresa = JSON.stringify(this.empresa); 
		console.log(jsonempresa);
		this.addEmpresa();
	}

	addEmpresa(){
		this._empresaSV.addEmpresa(this.empresa).subscribe(
			response =>{
				console.log("Guardado:D");
			},
			error =>{
				// console.log(<any>error);
				this.alertService.success({
	        		title: '¡Empresa guardada exitosamente!'
	     		 });
				this._router.navigate(['/empresas']);
			}
		);
	}

	getComisiones(){
		this._comisionSV.getComisionesActivas().subscribe(
			result => {
				// console.log(result);
				this.listaComisiones = result;
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getDepartamentos(){
		this._departamentoSV.getDepartamentos().subscribe(
			result => {
				// console.log(result);
				this.listaDepartamentos = result;
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getMunicipios(id){
		this._municipiosSV.getMunicipios(id).subscribe(
			result => {
				this.listaMunicipios = result;
				console.log(this.departamento);
			},
			error => {
				console.log(<any>error);
			}
			);
	}
	impresion(){
		console.log(this.municipio);
	}
}