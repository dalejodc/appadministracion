import { Component } from '@angular/core';
import { BarraNavegacionComponent } from './barraNavegacion.component';


@Component({
	selector: 'home',
	templateUrl: '../views/home.component.html'
})

export class HomeComponent{
	public titulo: string;

	constructor(){
		this.titulo = 'Pagina Home';
	}

	ngOnInit(){
		//alert("Home XD");
	}
}