import { Component } from '@angular/core';
import { SweetAlertService } from 'angular-sweetalert-service';
import { Router, ActivatedRoute, Params} from '@angular/router';

import { ComisionSV } from '../services/comision.service';
import { DepartamentoSV } from '../services/departamento.service';
import { MunicipiosSV } from '../services/municipios.service';
import { EmpresaSV } from '../services/empresa.service';
import { SolicitudSV } from '../services/solicitud.service';

import { Representante } from '../models/representante';
import { Departamento } from '../models/departamento';
import { Municipio } from '../models/municipio';
import { Comision } from '../models/comision';
import { Empresa } from '../models/empresa';
import { User } from '../models/user';
import { Solicitud } from '../models/solicitud';
import { Authorities } from '../models/authorities';
import { Inventario } from '../models/inventario';

@Component({
	selector: 'empresa-validacion',
	templateUrl: '../views/empresa-add.component.html',
	providers: [ComisionSV, DepartamentoSV, MunicipiosSV, EmpresaSV, SolicitudSV]
})

export class EmpresaValidacionComponent{

	public empresa: Empresa;
	public municipio: Municipio;
	public representante: Representante;
	public comision: Comision;
	public departamento: Departamento;
	public user: User;
	public listaAuthorities: Authorities[];
	public authorities: Authorities;
	public inventario: Inventario;
	public passAux: string;
	public solicitud: Solicitud;
	public id: number;


	public listaComisiones : Comision[];
	public listaMunicipios : Municipio[];
	public listaDepartamentos : Departamento[];

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _comisionSV: ComisionSV,
		private _departamentoSV: DepartamentoSV,
		private _municipiosSV: MunicipiosSV,
		private _empresaSV: EmpresaSV,
		private _solicitudSV: SolicitudSV,
		private alertService: SweetAlertService,
	){
		this.authorities = new Authorities(2, "ROLE_USER");
		this.listaAuthorities = [this.authorities]
		this.inventario = new Inventario(null, null);
		this.user = new User(null, null, true, this.listaAuthorities);
		this.representante = new Representante(null, null, null, null, null, true, this.user);
		this.departamento = new Departamento(null, null);
		this.municipio = new Municipio(0, null,this.departamento);
		this.comision = new Comision(null, null, null, null, true);
		this.empresa = new Empresa(null, null, null, null, true, this.comision, this.municipio, this.representante, this.inventario);
	}

	ngOnInit(){
		this.getSolicitud();
		this.getComisiones();
		this.getDepartamentos();
	}

	onSubmit(){
		console.log(this.empresa);
		let jsonempresa = JSON.stringify(this.empresa); 
		console.log(jsonempresa);
		this.addEmpresa();
		this.aprobarSolicitud();
	}

	getSolicitud(){
		this._route.params.forEach((params: Params)=>{
			let id = params['id'];
			this.id = params['id'];

			this._solicitudSV.getSolicitud(id).subscribe(
				response=>{
					console.log("Solicitud traida:", response);
					this.solicitud = response;
					this.empresa.nombre = response.nombreEmpresa;
					this.representante.nombre = response.nombreRepresentante;
					this.representante.cargo = response.cargo;
					this.representante.correo = response.correo;
					this.representante.telefono = response.telefono;
				},
				error=>{
					console.log(<any>error);
				}
			);
		});
	}

	aprobarSolicitud(){
		this.solicitud.estado.id = 2;

		this._solicitudSV.editSolicitud(this.id, this.solicitud).subscribe(
				response =>{
					console.log("Actualizado");
				},
				error =>{
					console.log(<any>error);
			}
		);
	}

	addEmpresa(){
		this._empresaSV.addEmpresa(this.empresa).subscribe(
			response =>{
				console.log("Guardado:D");
			},
			error =>{
				// console.log(<any>error);
				this.alertService.success({
	        		title: '¡Empresa guardada exitosamente!'
	     		 });
				this._router.navigate(['/empresas']);

			}
		);
	}

	getComisiones(){
		this._comisionSV.getComisionesActivas().subscribe(
			result => {
				// console.log(result);
				this.listaComisiones = result;
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getDepartamentos(){
		this._departamentoSV.getDepartamentos().subscribe(
			result => {
				// console.log(result);
				this.listaDepartamentos = result;
			},
			error => {
				console.log(<any>error);
			}
			);
	}

	getMunicipios(id){
		this._municipiosSV.getMunicipios(id).subscribe(
			result => {
				this.listaMunicipios = result;
				console.log(this.departamento);
			},
			error => {
				console.log(<any>error);
			}
			);
	}
	impresion(){
		console.log(this.municipio);
	}
}