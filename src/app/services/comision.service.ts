import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Comision } from '../models/comision';

@Injectable()
export class ComisionSV{

	public url = "http://localhost:8080/";
	private token = JSON.parse(sessionStorage.getItem('token')).access_token;

	private headers = new Headers({
		'Content-type': 'application/json',
		'Authorization': this.token
	});

	constructor(
		private _http: Http
	){
		console.log(sessionStorage.getItem('token'));
	}

	getComisiones(){
		return this._http.get(this.url+'comisiones', {headers: this.headers}).map(res=> res.json());
	}
	getComision(id){
		return this._http.get(this.url+'comisiones/'+id, {headers: this.headers}).map(res=> res.json());
	}

	getComisionesActivas(){
		return this._http.get(this.url+'comisiones/est/true', {headers: this.headers}).map(res=> res.json());
	}

	getComisionesDesactivadas(){
		return this._http.get(this.url+'comisiones/est/false', {headers: this.headers}).map(res=> res.json());
	}

	addComision(comision : Comision){
		let newcomision = JSON.stringify(comision); //Convirtiendo el objeto a JSON

		return this._http.post(this.url+'comisiones', newcomision, {headers:this.headers})
						 .map(res => res.json());
	}

	editComision(id, comision: Comision){
		let jsonComision = JSON.stringify(comision); //Convirtiendo el objeto a JSON

		return this._http.put(this.url+'comisiones/'+ id, jsonComision, {headers: this.headers})
						  .map(res => res.json());
	}

}
