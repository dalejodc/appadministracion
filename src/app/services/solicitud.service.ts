import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Solicitud } from '../models/solicitud';

@Injectable()
export class SolicitudSV{

	public url = "http://localhost:8080/";
	// public url = "http://192.168.43.92:8082/";

	constructor(
		private _http: Http
	){}

	getSolicitud(id){
		return this._http.get(this.url+'solicitudes/'+id).map(res=> res.json());
	}

	getSolicitudesPendientes(){
		return this._http.get(this.url+'solicitudes/est/1').map(res=> res.json());
	}

	getSolicitudesRechazadas(){
		return this._http.get(this.url+'solicitudes/est/3').map(res=> res.json());
	}

	deleteSolicitud(id){
		return this._http.delete(this.url+'solicitudes/'+ id +'/')
						  .map(res => res.json());	
	}

	editSolicitud(id, solicitud: Solicitud){
		let newsoli = JSON.stringify(solicitud); //Convirtiendo el objeto a JSON		
		let headers = new Headers({
			'Content-type':'application/json'
		});

		return this._http.put(this.url+'solicitudes/'+ id +'/', newsoli, {headers: headers})
						  .map(res => res.json());	
	}
}
