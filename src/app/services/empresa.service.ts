import  { Injectable } from '@angular/core';
import  { Component } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { Empresa } from '../models/empresa'
import { LoginSV } from '../services/login.service';
import { BarraNavegacionComponent } from '../components/barraNavegacion.component';

@Injectable()

export class EmpresaSV{

	public url = "http://localhost:8080/";
	public token : string;
	public x : BarraNavegacionComponent;
	public valor: boolean;

	constructor(
		private _http: Http,
	){
		// console.log("sessionStorage Empresa: ",sessionStorage.getItem('token'));
	}
	
	addEmpresa(empresa : Empresa){
		let jsonempresa = JSON.stringify(empresa); //Convirtiendo el objeto a JSON	
		let headers = new Headers({
			'Content-type':'application/json'
		});

		return this._http.post(this.url+'proveedores', jsonempresa, {headers:headers})
						 .map(res => res.json());
	} 

	editEmpresa(id, empresa: Empresa){
		let jsonEmpresa = JSON.stringify(empresa); //Convirtiendo el objeto a JSON		
		let headers = new Headers({
			'Content-type':'application/json'
		});

		return this._http.put(this.url+'proveedores/'+ id, jsonEmpresa, {headers: headers})
						  .map(res => res.json());	
	}

	getEmpresas(){

		// token = localStorage.getItem('token');

		let headers = new Headers({
			'Content-type':'application/json',
			// 'Authorization': ''
		});

		console.log("Token empresa:",localStorage.getItem('token'));

		// let options = new RequestOptions();
  //   		options.headers = new Headers();
  //   		options.headers.append('Authorization', localStorage.getItem('token'));
  //   		options.headers.append('Content-Type', 'application/json');

		return this._http.get(this.url+'proveedores', {headers: headers}).map(res=> res.json());
	}

	getEmpresa(id){
		return this._http.get(this.url+'proveedores/'+id).map(res=> res.json());
	}

	deleteEmpresa(id){
		return this._http.delete(this.url+'proveedores/'+ id +'/')
						  .map(res => res.json());	
	}

	setToken(token){
		this.token = token;
	}
}