import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DepartamentoSV{

	public url = "http://localhost:8080/";

	constructor(
		private _http: Http
	){}

	getDepartamentos(){
		return this._http.get(this.url+'departamentos').map(res=> res.json());
	}
}