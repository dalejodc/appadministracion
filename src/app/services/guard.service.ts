import  { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';

@Injectable () 

export class AuthGuardService implements CanActivate { 

	constructor(){
	}

  canActivate ( route : ActivatedRouteSnapshot , state : RouterStateSnapshot ): boolean { 
  	//true deja entrar, false niega el acceso
  	// return this._loginSV.isAuthenticated(); 
  	// return true ; 
  	if(sessionStorage.getItem('token')){
		return true;
	}else{
		return false;
	}
  }

}