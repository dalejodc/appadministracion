import  { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MunicipiosSV{

	public url = "http://localhost:8080/";

	constructor(
		private _http: Http
	){}

	getMunicipios(id){
		return this._http.get(this.url+'/municipios/dep/'+ id).map(res=> res.json());
	}
}
