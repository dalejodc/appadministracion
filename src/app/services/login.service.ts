import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { User } from '../models/user';


@Injectable()
export class LoginSV {

  public url = "http://localhost:8080/";
  public token: string;
  public user: User;

  public mensaje: String;
  public errMsg: boolean;

	constructor(
		private _http: Http,
		private _router: Router
	){
		// this.token ="vacio";
	}

  login(user: User) {
    let jsonUser = JSON.stringify(user); //Convirtiendo el objeto a JSON
    let headers = new Headers({
      'Content-type': 'application/json'
    });

    return this._http.post(this.url + 'auth/login', jsonUser, { headers: headers })
      .map(res => res.json())
      .subscribe(
      data => {
        sessionStorage.setItem('user', data.user.username);
        console.log("Usuario Loggeado: ", sessionStorage.getItem("user"));
        sessionStorage.setItem('usuario', JSON.stringify(data.user));
        sessionStorage.setItem('rol', data.user.authorities[0].id);
        sessionStorage.setItem('token', JSON.stringify(data.tokenState));

        var rol = sessionStorage.getItem('rol');

        if (rol == '2') {
          console.log("es un admin");
					this.errMsg = false;
          this._router.navigate(['/home']);
        }
        else {
          console.log("no es un admin");
          this.errMsg = true;
        }
      },
      error => {
        this.errMsg = true;
      }
		);
  }

  getMensajeError() {
    return this.errMsg;
  }


}
