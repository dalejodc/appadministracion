/*=============================================================================================
              IMPORTAR DEPENDENCIAS
===============================================================================================*/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; //para poder hacer binding de inputs
import { HttpModule } from '@angular/http'; //Para usar peticiones
import { MaterializeModule } from 'angular2-materialize';
import { SweetAlertService } from 'angular-sweetalert-service';
import { AppComponent } from './app.component';
import { routing, appRoutingProviders} from "./app.routing"; //Para las rutas

/*=============================================================================================
              IMPORTAR COMPONENTES CREADOS
===============================================================================================*/
import { HomeComponent } from './components/home.component';
import { ErrorComponent } from './components/error.component';
import { BarraNavegacionComponent } from './components/barraNavegacion.component';
import { LoginComponent } from './components/login.component';
import { SolicitudComponent } from './components/solicitud.component';
import { EmpresaComponent } from './components/empresa.component';
import { EmpresaAddComponent } from './components/empresa-add.component';
import { EmpresaEditComponent } from './components/empresa-edit.component';
import { EmpresaValidacionComponent } from './components/empresa-validacion.component';
import { ComisionComponent } from './components/comision.component';
import { ComisionAddComponent } from './components/comision-add.component';
import { ComisionEditComponent } from './components/comision-edit.component';
import { AuthGuardService } from './services/guard.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ErrorComponent,
    BarraNavegacionComponent,
    LoginComponent,
    SolicitudComponent,
    EmpresaComponent,
    EmpresaAddComponent,
    EmpresaValidacionComponent,
    EmpresaEditComponent,
    ComisionComponent,
    ComisionAddComponent,
    ComisionEditComponent
  ],
  imports: [
    BrowserModule,
    MaterializeModule,
    routing,
    FormsModule,
    HttpModule,
  ],
  providers: [ appRoutingProviders, SweetAlertService, AuthGuardService],
  bootstrap: [ AppComponent]
})
export class AppModule { }
