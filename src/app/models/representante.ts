import { User } from '../models/user';

export class Representante{
	constructor(
		public id: number,
		public nombre: string,
		public cargo : string,
		public correo: string,
		public telefono: string,
		public estado: boolean,
		public user: User
	){}
}