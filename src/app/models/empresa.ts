import { Comision } from './comision';
import { Municipio } from './municipio';
import { Representante } from './representante';
import { Inventario } from '../models/inventario';

export class Empresa {
	
	constructor(
		public id: number,
		public nombre: string,
		public direccion: string,
		public nit: string,
		public estado: boolean,
		public comision: Comision,
		public municipio: Municipio,
		public representante: Representante,
		public inventario: Inventario
	){}

}