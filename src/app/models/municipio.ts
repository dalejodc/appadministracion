import { Departamento } from './departamento'

export class Municipio{

	constructor(
		public munCodigo: number,
		public munNombre: string,
		public departamento: Departamento
	){}
}