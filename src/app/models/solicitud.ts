import { Estado } from '../models/estado';

export class Solicitud{

	constructor(
		public id: number,
		public nombreEmpresa: string,
		public nombreRepresentante: string,
		public cargo: string,
		public correo: string,
		public telefono: string,
		public estado: Estado
		){}
}