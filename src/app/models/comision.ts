export class Comision{

	constructor(
		public id: number,
		public nombre: string,
		public descripcion: string,
		public monto: number,
		public estado: boolean,
	){}
}