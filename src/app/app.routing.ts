/*=============================================================================================
              IMPORTAR DEPENDENCIAS
===============================================================================================*/
import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule} from "@angular/router";

/*=============================================================================================
              IMPORTAR COMPONENTES CREADOS
===============================================================================================*/
import { HomeComponent } from './components/home.component';
import { ErrorComponent } from './components/error.component';
import { LoginComponent } from './components/login.component';
import { SolicitudComponent } from './components/solicitud.component';
import { EmpresaComponent } from './components/empresa.component';
import { EmpresaAddComponent } from './components/empresa-add.component';
import { EmpresaEditComponent } from './components/empresa-edit.component';
import { EmpresaValidacionComponent } from './components/empresa-validacion.component';
import { ComisionComponent } from './components/comision.component';
import { ComisionAddComponent } from './components/comision-add.component';
import { ComisionEditComponent } from './components/comision-edit.component';
import { AuthGuardService } from './services/guard.service';

/*=============================================================================================
              ARREGLO DE LAS RUTAS
===============================================================================================*/
const appRoutes: Routes = [
	{path: '', component: LoginComponent}, //Página Home, ruta inicial

	{path: 'home', component: HomeComponent , canActivate:[AuthGuardService]},
	{path: 'login', component: LoginComponent },
	{path: 'solicitudes', component: SolicitudComponent},
	{path: 'empresas', component: EmpresaComponent, canActivate:[AuthGuardService]},
	{path: 'empresa-add', component: EmpresaAddComponent , canActivate:[AuthGuardService]},
	{path: 'comisiones', component: ComisionComponent , canActivate:[AuthGuardService]},
	{path: 'comision-add', component: ComisionAddComponent , canActivate:[AuthGuardService]},
	{path: 'comisiones/comision-edit/:id', component: ComisionEditComponent , canActivate:[AuthGuardService]},
	{path: 'empresas/empresa-edit/:id', component: EmpresaEditComponent , canActivate:[AuthGuardService]},
	{path: 'solicitudes/empresa-validacion/:id', component: EmpresaValidacionComponent , canActivate:[AuthGuardService]},

	{path: '**', component: ErrorComponent} //Página para errores, cuando la ruta falla
]


//Procedimiento que necesita Angular
export const appRoutingProviders: any[] = [];
/*
-Variable routing de tipo ModuleWithProviders
-Con el método .forRoot le decimos qué arreglo tiene que cargar para las rutas, tiene que ser el
que habíamos creado antes. 
-Agara todas las rutas que le hemos indicado, las introduzca y las inyecte en la configuración de rutas del 
framework */
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);